package in.project.sensor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.project.sensor.datamodels.detailProject.DetailResponse;
import in.project.sensor.datamodels.projectmodels.ProjectModel;
import in.project.sensor.utils.AppController;
import in.project.sensor.utils.ConstantFunctions;
import in.project.sensor.utils.IntentData;
import in.project.sensor.utils.WebService;

public class DetailedUserInfo extends AppCompatActivity {

    @BindView(R.id.txtProjectTitle)
    TextView txtProjectTitle;
    @BindView(R.id.txtProjectDesc)
    TextView txtProjectDesc;
    @BindView(R.id.txtProjectStatus)
    TextView txtProjectStatus;
    @BindView(R.id.txtTemp)
    TextView txtTemp;
    @BindView(R.id.txtWeight)
    TextView txtWeight;
    @BindView(R.id.txtPressure)
    TextView txtPressure;
    @BindView(R.id.txtGasPresent)
    TextView txtGasPresent;
    @BindView(R.id.cardProject)
    LinearLayout cardProject;
    @BindView(R.id.layoutDetailInfo)
    LinearLayout layoutDetailInfo;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.txtUserEmail)
    TextView txtUserEmail;
    @BindView(R.id.txtUserMobile)
    TextView txtUserMobile;
    @BindView(R.id.txtUserAddress)
    TextView txtUserAddress;
    private Activity activity = DetailedUserInfo.this;
    private String projectId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_user_info);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        setTitle("Project Info");
        projectId = getIntent().getStringExtra(IntentData.KEY_PROJECT_ID);


    }


    @Override
    protected void onResume() {
        super.onResume();
        onGetDetailInfo();
    }

    @OnClick(R.id.txtProjectStatus)
    public void onViewClicked() {
    }


    private void onGetDetailInfo() {
        String urlAddress = WebService.API_GET_DETAILED_PROJECTS + "/" + projectId;
        StringRequest myOrderRequest = new StringRequest(Request.Method.GET, urlAddress,
                this::onSetDetailInfo, error -> Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show()) {
        };

        AppController.getInstance().addToRequestQueue(myOrderRequest);
    }

    private void onSetDetailInfo(String response) {
        DetailResponse detailResponse = new Gson().fromJson(response, DetailResponse.class);
        if (detailResponse.getCode()) {
            ProjectModel projectModel = detailResponse.getData();
            layoutDetailInfo.setVisibility(View.VISIBLE);
            txtUserName.setText("NAME :- " + projectModel.getUserModel().getName());
            txtUserEmail.setText("EMAIL :- " + projectModel.getUserModel().getEmail());
            txtUserMobile.setText("MOBILE :- " + projectModel.getUserModel().getMobile());
            txtUserAddress.setText("ADDRESS :- " + projectModel.getUserModel().getAddress());


            txtProjectTitle.setText(projectModel.getProjectName());
            txtProjectDesc.setText(projectModel.getProjectDesc());

            txtTemp.setText(projectModel.getSensorValOne()+" °C");
            txtWeight.setText(projectModel.getSensorValTwo()+ " Kg");
            txtPressure.setText(projectModel.getSensorValThree()+ " Pa");
            txtGasPresent.setText(projectModel.getSensorValFour()+" kWh");
            if (projectModel.getProjectStatus()) {
                cardProject.setBackground(getResources().getDrawable(R.drawable.sixbg));
                txtProjectStatus.setTextColor(getResources().getColor(R.color.green_color));
                txtProjectStatus.setText("WORKING MODE ON");
                txtProjectStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_working, 0, 0, 0);

            } else {

                ConstantFunctions.OnShowAlertMessage(activity, "Temperature rise please check on the planet");
                txtProjectStatus.setText("WORKING MODE OFF");
                cardProject.setBackground(getResources().getDrawable(R.drawable.fivebg));
                txtProjectStatus.setTextColor(getResources().getColor(R.color.colorRed));
                txtProjectStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_not_working, 0, 0, 0);
            }

        } else {
            layoutDetailInfo.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
