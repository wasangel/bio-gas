package in.project.sensor.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    private static final String SP_MAIN_PROFILE = "SP_MAIN_PROFILE";
    private static final String SP_PROFILE_USERNAME = "SP_PROFILE_USERNAME";
    private static final String SP_PROFILE_USER_ID = "SP_PROFILE_USER_ID";
    private static final String SP_PROFILE_USERPASS = "SP_PROFILE_USERPASS";
    private static final String SP_PROFILE_USER_ROLE = "SP_PROFILE_USER_ROLE";
    private static final String SP_PROFILE_USER_LOGGED = "SP_PROFILE_USER_LOGGED";
    private static final String SP_TEMP_CUSTOMER_LOGGED_IN = "SP_TEMP_CUSTOMER_LOGGED_IN";
    private static final String SP_PROFILE_MOBILE = "SP_PROFILE_MOBILE";
    private static final String SP_PROFILE_EMAIL = "SP_PROFILE_EMAIL";
    private static final String SP_CUSTOMER_DETAILS = "SP_CUSTOMER_DETAILS";
    private static final String SP_PROFILE_USER_TOKEN = "SP_USER_TOKEN";
    private static final String SP_PROFILE_USER_PINCODE_CHOOSE = "SP_PROFILE_USER_PINCODE_CHOOSE";
    private static final String SP_PROFILE_USER_PINCODE = "SP_PROFILE_USER_PINCODE";
    private static final String SP_PROFILE_USER_PINCODE_DELIVERY_CHARGE = "SP_PROFILE_USER_PINCODE_DELIVERY_CHARGE";
    private static final String SP_PROFILE_USER_NAME = "SP_PROFILE_USER_NAME";
    private static final String SP_PROFILE_USER_PINCODE_DELIVERY_MIN_AMT = "SP_PROFILE_USER_PINCODE_DELIVERY_MIN_AMT";
    private SharedPreferences.Editor editor;
    private SharedPreferences preferences;
    private Context context;


    private static final String SP_USER_NAME = "SP_USER_NAME";
    private static final String SP_USER_MOBILE = "SP_USER_MOBILE";
    private static final String SP_USER_PASSWORD = "SP_USER_PASSWORD";
    private static final String SP_USER_EMAIL = "SP_USER_EMAIL";

    private String SP_TEMP_CUSTOMER_ID = "temp_customer_id";

    public SessionManager(Context context) {
        this.context = context;
        editor = context.getSharedPreferences(SP_MAIN_PROFILE, Context.MODE_PRIVATE).edit();
        preferences = context.getSharedPreferences(SP_MAIN_PROFILE, Context.MODE_PRIVATE);
        editor.apply();
    }


    public void onChoosedPinCode(boolean isChoosen, String pincode, int deliveryCharge, int minAmt) {

        editor.putString(SP_PROFILE_USER_PINCODE, pincode);
        editor.putBoolean(SP_PROFILE_USER_PINCODE_CHOOSE, true);
        editor.putInt(SP_PROFILE_USER_PINCODE_DELIVERY_CHARGE, deliveryCharge);
        editor.putInt(SP_PROFILE_USER_PINCODE_DELIVERY_MIN_AMT, minAmt);
        editor.apply();
    }

    public int getDeliveryMinAmt() {
        return preferences.getInt(SP_PROFILE_USER_PINCODE_DELIVERY_MIN_AMT, 0);
    }

    public boolean isPincodeChoosen() {
        return preferences.getBoolean(SP_PROFILE_USER_PINCODE_CHOOSE, false);
    }

    public String getPincode() {
        return preferences.getString(SP_PROFILE_USER_PINCODE, "NA");
    }

    public int getDeliveryCharge() {
        return preferences.getInt(SP_PROFILE_USER_PINCODE_DELIVERY_CHARGE, 0);
    }


    public void onCreateLogin(String userToken, String userId, String userName, String role) {

        editor.putString(SP_PROFILE_USER_ID, userId);
        editor.putString(SP_PROFILE_USER_TOKEN, userToken);
        editor.putString(SP_PROFILE_USER_NAME, userName);
        editor.putString(SP_PROFILE_USER_ROLE, role);
        editor.putBoolean(SP_PROFILE_USER_LOGGED, true);
        editor.apply();
    }

    public String getSpProfileUsername() {
        return preferences.getString(SP_PROFILE_USER_NAME, "");
    }

    public void StoreTempUserId(long tempId) {
        editor.putLong(SP_TEMP_CUSTOMER_ID, tempId);
        editor.putBoolean(SP_TEMP_CUSTOMER_LOGGED_IN, true);
        editor.apply();
    }

    public String getUserName() {
        return preferences.getString(SP_PROFILE_USER_NAME, "");
    }

    public String getMobile() {
        return preferences.getString(SP_PROFILE_MOBILE, "");
    }

    public String getEmail() {
        return preferences.getString(SP_PROFILE_EMAIL, "");
    }


    public String checkUserRole() {
        return preferences.getString(SP_PROFILE_USER_ROLE, "");
    }


    public long getTempUserId() {
        return preferences.getLong(SP_TEMP_CUSTOMER_ID, 0);
    }

    public boolean isTempLoggedIn() {
        return preferences.getBoolean(SP_TEMP_CUSTOMER_LOGGED_IN, false);
    }


    public boolean isLoggedIn() {
        return preferences.getBoolean(SP_PROFILE_USER_LOGGED, false);
    }

    public String getUserToken() {
        return preferences.getString(SP_PROFILE_USER_TOKEN, "");
    }

    public String getUserId() {
        return preferences.getString(SP_PROFILE_USER_ID, "");
    }

    public void onLogoutUser() {
        editor.putBoolean(SP_PROFILE_USER_LOGGED, false);
        editor.clear();
        editor.apply();
    }

    public void setCustomerDetails(String customerInfo) {
        editor.putString(SP_CUSTOMER_DETAILS, customerInfo);
        editor.apply();
    }

    public String getSpCustomerDetails() {
        return preferences.getString(SP_CUSTOMER_DETAILS, "NA");
    }

    public String getSpUserName() {
        return preferences.getString(SP_USER_NAME, "NA");
    }

    public String getSpUserMobile() {
        return preferences.getString(SP_USER_MOBILE, "NA");

    }

    public String getSpUserPassword() {
        return preferences.getString(SP_USER_PASSWORD, "NA");
    }

    public String getSpUserEmail() {
        return preferences.getString(SP_USER_EMAIL, "NA");
    }

    public void saveUser(String userName, String userPass, String userEmail, String userMobile) {
        editor.putString(SP_USER_NAME, userName);
        editor.putString(SP_USER_EMAIL, userEmail);
        editor.putString(SP_USER_MOBILE, userMobile);
        editor.putString(SP_USER_PASSWORD, userPass);
        editor.apply();

    }
}
