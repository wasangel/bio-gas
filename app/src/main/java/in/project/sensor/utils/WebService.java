package in.project.sensor.utils;


public class WebService {

    public static final String API_KEY = "p2lbgWkFrykA4QyUmpHihzmc5BNzIABq";

    public static final String API_BASE_URL = "http://192.168.0.106/api/";

    public static final String API_LOGIN = API_BASE_URL + "login";
    public static final String API_REGISTER = API_BASE_URL + "register";


    public static final String PARAM_NAME = "name";
    public static final String PARAM_KEY_EMAIL = "email";
    public static final String PARAM_KEY_MOBILE = "mobile";
    public static final String PARAM_KEY_ADDRESS = "address";

    public static final String SP_FCM_MAIN = "SP_FCM_MAIN";
    public static final String FCM_TOKEN = "FCM_TOKEN";


    public static final String PARAM_KEY_PASSWORD = "password";
    public static final String PARAM_C_PASSWORD = "c_password";

    public static final String PARAM_KEY_FCM_TOKEN = "fcm_token";
    public static final String API_DATA_AUTHORIZATION = "Authorization";
    public static final String API_DATA_ACCEPT_APPLICATION_JSON = "application/json";
    public static final String APP_API_KEY = "API_KEY";
    public static final String API_DATA_ACCEPT = "Accept";

    public static final String API_GET_MY_PROJECTS = API_BASE_URL + "myProjects";
    public static final String API_GET_USER_PROJECTS = API_BASE_URL + "userproject";
    public static final String API_GET_USERS = API_BASE_URL + "users";
    public static final String API_GET_DETAILED_PROJECTS = API_BASE_URL + "detailproject";
}
