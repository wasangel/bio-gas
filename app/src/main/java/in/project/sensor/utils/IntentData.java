package in.project.sensor.utils;

public class IntentData {

    public static final String KEY_USER_ID = "KEY_USER_ID";
    public static final String KEY_USER_NAME = "KEY_USER_NAME";
    public static final String KEY_PROJECT_ID = "KEY_PROJECT_ID";
}
