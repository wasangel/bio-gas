package in.project.sensor.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.project.sensor.R;


public class ConstantFunctions {

    public static final String INDIAN_MOBILE_NUMBER_PATTERN = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";


    public static boolean onCheckInternet(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {

            return true;
        } else {

            Toast.makeText(context, "Internet is not Available Now. Please Turn on internet/wifi and restart application.", Toast.LENGTH_LONG).show();
            return false;
        }

    }

    public static void OnShowAlertMessage(final Context context, String message) {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(context);
        myDialog.setTitle("");
        myDialog.setMessage(message);
        myDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        myDialog.create();
        myDialog.show();
    }

    public static void onGotoContextAlert(final Context context, String message, final Activity activity) {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(context);
        myDialog.setTitle("");
        myDialog.setMessage(message);
        myDialog.setCancelable(false);


        myDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                context.startActivity(new Intent(context, activity.getClass())
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));

            }
        });

        myDialog.create();
        myDialog.show();
    }

    public static void onGotoContextBackAlert(final Context context, String message, final Activity activity) {
        AlertDialog.Builder myDialog = new AlertDialog.Builder(context);
        myDialog.setTitle("");
        myDialog.setMessage(message);


        myDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                context.startActivity(new Intent(context, activity.getClass())
                );

            }
        });

        myDialog.setOnKeyListener((dialog, keyCode, event) -> {

            if (keyCode == KeyEvent.KEYCODE_BACK) {

                //Toast.makeText(context, "Please Wait..", Toast.LENGTH_SHORT).show();
                // dialog.dismiss();
                return false;
                //return true;

            } else {
                dialog.dismiss(); // dismiss the dialog
                return true;
            }

        });

        myDialog.create();
        myDialog.show();
    }

    public static Dialog onShowProgressDialog(final Context context) {
        Dialog alertDialog = new Dialog(context);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.progresslayout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();


        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

//                    Toast.makeText(context, "Please Wait..", Toast.LENGTH_SHORT).show();
                    return false;
//                    dialog.dismiss();
                    // return true;

                } else {
                    dialog.dismiss(); // dismiss the dialog
                    return true;
                }


            }
        });


        return alertDialog;
    }

    public static void hideKeyBoard(Activity context) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager.isAcceptingText()) {
            inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }

//    public static void checkPermission(Activity context) {
//        int PERMISSION_ALL = 100;
//        String[] PERMISSIONS = {
//                Manifest.permission.READ_SMS,
//                Manifest.permission.RECEIVE_SMS,
//        };
//
//        if (!hasPermissions(context, PERMISSIONS)) {
//            ActivityCompat.requestPermissions(context, PERMISSIONS, PERMISSION_ALL);
//        }
//    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static String getTextInColours(String text, int textColor) {

        return String.valueOf(Html.fromHtml("<font color='" +
                "#4CAF50" + "'>" +
                text + " </font>"));
    }


    public static String getUPIString(String payeeAddress, String payeeName, String payeeMCC, String trxnID, String trxnRefId,
                                      String trxnNote, String payeeAmount, String currencyCode, String refUrl) {
        String UPI = "upi://pay?pa=" + payeeAddress + "&pn=" + payeeName
                + "&mc=" + payeeMCC + "&tid=" + trxnID + "&tr=" + trxnRefId
                + "&tn=" + trxnNote + "&am=" + payeeAmount + "&cu=" + currencyCode
                + "&refUrl=" + refUrl;
        return UPI.replace(" ", "+");
    }

    public static String generateRandom() {
        String aToZ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; // 36 letter.
        Random rand = new Random();
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            int randIndex = rand.nextInt(aToZ.length());
            res.append(aToZ.charAt(randIndex));
        }
        return res.toString();
    }

    public static String getDateString(long timeInMilliseconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy 'At' HH:mm a");
        return formatter.format(timeInMilliseconds);
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
