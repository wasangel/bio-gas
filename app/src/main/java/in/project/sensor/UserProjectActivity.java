package in.project.sensor;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.project.sensor.adaptors.MyProjectAdaptor;
import in.project.sensor.utils.AppController;
import in.project.sensor.utils.IntentData;
import in.project.sensor.utils.WebService;
import in.project.sensor.datamodels.projectmodels.ProjectResponse;

public class UserProjectActivity extends AppCompatActivity {

    @BindView(R.id.recyclerMyProjects)
    RecyclerView recyclerMyProjects;
    @BindView(R.id.swipeMyProjects)
    SwipeRefreshLayout swipeMyProjects;
    private Activity activity = UserProjectActivity.this;
    private String userId, userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_project);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);


        userId = getIntent().getStringExtra(IntentData.KEY_USER_ID);
        userName = getIntent().getStringExtra(IntentData.KEY_USER_NAME);
        setTitle(userName);

        Log.d("USER_ID", "onCreate: "+userId);

        RecyclerView.LayoutManager cartLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerMyProjects.setLayoutManager(cartLayoutManager);
        recyclerMyProjects.hasFixedSize();
        recyclerMyProjects.setItemAnimator(new DefaultItemAnimator());
        recyclerMyProjects.setHasFixedSize(true);
        recyclerMyProjects.setItemViewCacheSize(500);


        swipeMyProjects.setOnRefreshListener(this::onGetUserProject);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onGetUserProject();
    }

    private void onGetUserProject() {
        String urlAddress = WebService.API_GET_USER_PROJECTS + "/" + userId;
        Log.d("USER_ID", "onGetUserProject: "+urlAddress);
        StringRequest myOrderRequest = new StringRequest(Request.Method.GET, urlAddress,
                this::onSetUserOrder, error -> Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show()) {
        };

        AppController.getInstance().addToRequestQueue(myOrderRequest);
    }

    private void onSetUserOrder(String response) {
        Log.d("RESPONSE", "onSetMyOrders: " + response);
        if (swipeMyProjects.isRefreshing()) {
            swipeMyProjects.setRefreshing(false);
        }
        ProjectResponse projectResponse = new Gson().fromJson(response, ProjectResponse.class);
        if (projectResponse.getCode()) {
            recyclerMyProjects.setAdapter(new MyProjectAdaptor(projectResponse.getData(), activity));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
