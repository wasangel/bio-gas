package in.project.sensor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.project.sensor.adaptors.MyProjectAdaptor;
import in.project.sensor.datamodels.projectmodels.ProjectResponse;
import in.project.sensor.utils.AppController;
import in.project.sensor.utils.ConstantFunctions;
import in.project.sensor.utils.SessionManager;
import in.project.sensor.utils.WebService;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recyclerMyProjects)
    RecyclerView recyclerMyProjects;
    @BindView(R.id.swipeMyProjects)
    SwipeRefreshLayout swipeMyProjects;
    private Activity activity = MainActivity.this;
    private SessionManager mySessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        setTitle("Dashboard");
        mySessionManager = new SessionManager(activity);


        RecyclerView.LayoutManager cartLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerMyProjects.setLayoutManager(cartLayoutManager);
        recyclerMyProjects.hasFixedSize();
        recyclerMyProjects.setItemAnimator(new DefaultItemAnimator());
        recyclerMyProjects.setHasFixedSize(true);
        recyclerMyProjects.setItemViewCacheSize(500);

        mySessionManager = new SessionManager(activity);

        swipeMyProjects.setOnRefreshListener(this::onGetMyProjects);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onGetMyProjects();
    }

    private void onGetMyProjects() {

        Dialog dialog = ConstantFunctions.onShowProgressDialog(activity);
        String urlAddress = WebService.API_GET_MY_PROJECTS;
        StringRequest myOrderRequest = new StringRequest(Request.Method.GET, urlAddress,
                response -> {
                    dialog.dismiss();
                    onSetMyOrders(response);
                }, error -> {
            dialog.dismiss();
            Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<>();
                param.put(WebService.APP_API_KEY, WebService.API_KEY);
                param.put(WebService.API_DATA_ACCEPT, WebService.API_DATA_ACCEPT_APPLICATION_JSON);
                param.put(WebService.API_DATA_AUTHORIZATION, "Bearer " + mySessionManager.getUserToken());
                return param;
            }

        };

        AppController.getInstance().addToRequestQueue(myOrderRequest);
    }

    private void onSetMyOrders(String response) {
        Log.d("RESPONSE", "onSetMyOrders: " + response);
        if (swipeMyProjects.isRefreshing()) {
            swipeMyProjects.setRefreshing(false);
        }

        ProjectResponse projectResponse = new Gson().fromJson(response, ProjectResponse.class);
        if (projectResponse.getCode()) {
            recyclerMyProjects.setAdapter(new MyProjectAdaptor(projectResponse.getData(), activity));
        }

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            mySessionManager.onLogoutUser();
            startActivity(new Intent(activity, LoginActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
