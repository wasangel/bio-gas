package in.project.sensor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import in.project.sensor.utils.ConstantFunctions;
import in.project.sensor.utils.SessionManager;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_TIME_OUT = 1000;
    private SessionManager mySessionManager;
    private Activity activity = SplashActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        mySessionManager = new SessionManager(activity);
//        FirebaseApp.initializeApp(this);
//        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//            @Override
//            public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                if (task.isSuccessful()) {
//
//                    String fmToken = task.getResult().getToken();
//                    SharedPreferences.Editor editor = getSharedPreferences(WebService.SP_FCM_MAIN, MODE_PRIVATE).edit();
//                    editor.putString(WebService.FCM_TOKEN, "");
//                    editor.apply();
//                    Log.d("FCM_TOKEN", "onComplete: " + fmToken);
//                    //Toast.makeText(activity, fmToken, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        if (ConstantFunctions.onCheckInternet(activity)) {
            new Handler().postDelayed(() -> {
                if (mySessionManager.isLoggedIn()) {

                    if (mySessionManager.checkUserRole().equals("customer")) {
                        startActivity(new Intent(activity, MainActivity.class));

                    } else if (mySessionManager.checkUserRole().equals("admin")) {
                        startActivity(new Intent(activity, AdminDashboard.class));
                    }
                } else {
                    startActivity(new Intent(activity, LoginActivity.class));
                }

                finish();
            }, SPLASH_TIME_OUT);

        }

    }
}
