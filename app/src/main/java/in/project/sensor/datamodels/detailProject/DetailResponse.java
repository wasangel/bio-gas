package in.project.sensor.datamodels.detailProject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.project.sensor.datamodels.projectmodels.ProjectModel;

public class DetailResponse {

    @SerializedName("code")
    @Expose
    private Boolean code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ProjectModel data;

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProjectModel getData() {
        return data;
    }

    public void setData(ProjectModel data) {
        this.data = data;
    }

}