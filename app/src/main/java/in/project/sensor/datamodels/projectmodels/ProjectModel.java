package in.project.sensor.datamodels.projectmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.project.sensor.datamodels.userresponse.UserModel;

public class ProjectModel {

    @SerializedName("customer_info")
    @Expose
    private UserModel userModel;

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    @SerializedName("project_id")
    @Expose
    private String project_id;

    @SerializedName("project_name")
    @Expose
    private String projectName;

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    @SerializedName("project_desc")
    @Expose
    private String projectDesc;
    @SerializedName("project_status")
    @Expose
    private Boolean projectStatus;
    @SerializedName("sensor_val_one")
    @Expose
    private String sensorValOne;
    @SerializedName("sensor_val_two")
    @Expose
    private String sensorValTwo;
    @SerializedName("sensor_val_three")
    @Expose
    private String sensorValThree;
    @SerializedName("sensor_val_four")
    @Expose
    private String sensorValFour;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDesc() {
        return projectDesc;
    }

    public void setProjectDesc(String projectDesc) {
        this.projectDesc = projectDesc;
    }

    public Boolean getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(Boolean projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getSensorValOne() {
        return sensorValOne;
    }

    public void setSensorValOne(String sensorValOne) {
        this.sensorValOne = sensorValOne;
    }

    public String getSensorValTwo() {
        return sensorValTwo;
    }

    public void setSensorValTwo(String sensorValTwo) {
        this.sensorValTwo = sensorValTwo;
    }

    public String getSensorValThree() {
        return sensorValThree;
    }

    public void setSensorValThree(String sensorValThree) {
        this.sensorValThree = sensorValThree;
    }

    public String getSensorValFour() {
        return sensorValFour;
    }

    public void setSensorValFour(String sensorValFour) {
        this.sensorValFour = sensorValFour;
    }

}