package in.project.sensor.datamodels.projectmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProjectResponse {

    @SerializedName("code")
    @Expose
    private Boolean code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<ProjectModel> data = null;

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProjectModel> getData() {
        return data;
    }

    public void setData(List<ProjectModel> data) {
        this.data = data;
    }

}