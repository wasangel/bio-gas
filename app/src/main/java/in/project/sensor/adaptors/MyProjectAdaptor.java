package in.project.sensor.adaptors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.project.sensor.DetailedUserInfo;
import in.project.sensor.R;
import in.project.sensor.datamodels.projectmodels.ProjectModel;
import in.project.sensor.utils.ConstantFunctions;
import in.project.sensor.utils.IntentData;

public class MyProjectAdaptor extends RecyclerView.Adapter<MyProjectAdaptor.MyViewHolder> {
    private List<ProjectModel> projectList;
    private Context context;

    public MyProjectAdaptor(List<ProjectModel> projectList, Context context) {
        this.projectList = projectList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_project_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        ProjectModel projectModel = projectList.get(position);
        holder.txtProjectTitle.setText(projectModel.getProjectName());
        holder.txtProjectDesc.setText(projectModel.getProjectDesc());
        if (projectModel.getProjectStatus()) {
            holder.cardProject.setBackground(context.getResources().getDrawable(R.drawable.sixbg));
            holder.txtProjectStatus.setTextColor(context.getResources().getColor(R.color.green_color));
            holder.txtProjectStatus.setText("WORKING MODE ON");
            holder.txtProjectStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_working, 0, 0, 0);

        } else {
            holder.txtProjectStatus.setText("WORKING MODE OFF");
            holder.cardProject.setBackground(context.getResources().getDrawable(R.drawable.fivebg));
            holder.txtProjectStatus.setTextColor(context.getResources().getColor(R.color.colorRed));
            holder.txtProjectStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_not_working, 0, 0, 0);
        }
        holder.cardProject.setOnClickListener(v -> context.startActivity(new Intent(context, DetailedUserInfo.class).putExtra(IntentData.KEY_PROJECT_ID, projectModel.getProject_id())));
        holder.txtProjectStatus.setOnClickListener(v -> {
            if (projectModel.getProjectStatus()) {
                ConstantFunctions.OnShowAlertMessage(context, "Application is safe from FAILURE");
            } else {
                ConstantFunctions.OnShowAlertMessage(context, "Temperature rise please check on the planet");
            }
        });

    }

    @Override
    public int getItemCount() {
        return projectList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtProjectTitle, txtProjectDesc, txtProjectStatus;
        LinearLayout cardProject;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtProjectTitle = itemView.findViewById(R.id.txtProjectTitle);
            txtProjectDesc = itemView.findViewById(R.id.txtProjectDesc);
            txtProjectStatus = itemView.findViewById(R.id.txtProjectStatus);
            cardProject = itemView.findViewById(R.id.cardProject);
        }
    }
}
