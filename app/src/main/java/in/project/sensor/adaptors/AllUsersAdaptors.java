package in.project.sensor.adaptors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.project.sensor.R;
import in.project.sensor.UserProjectActivity;
import in.project.sensor.utils.IntentData;
import in.project.sensor.datamodels.userresponse.UserModel;

public class AllUsersAdaptors extends RecyclerView.Adapter<AllUsersAdaptors.MyViewHolder> {
    private Context context;
    private List<UserModel> userList;

    public AllUsersAdaptors(Context context, List<UserModel> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_users_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        UserModel userModel = userList.get(position);
        holder.txtUserName.setText(userModel.getName() + " (" + userModel.getMobile() + " )");
        holder.txtUserName.setOnClickListener(v -> context.startActivity(new Intent(context, UserProjectActivity.class)
                .putExtra(IntentData.KEY_USER_ID, String.valueOf(userModel.getId()))
                .putExtra(IntentData.KEY_USER_NAME, userModel.getName())
        ));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtUserName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName = itemView.findViewById(R.id.txtUserName);
        }
    }
}
