package in.project.sensor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.project.sensor.utils.AppController;
import in.project.sensor.utils.ConstantFunctions;
import in.project.sensor.utils.SessionManager;
import in.project.sensor.utils.WebService;
import in.project.sensor.datamodels.authmodels.AuthResponseModel;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.etxLoginEmail)
    AppCompatEditText etxLoginEmail;
    @BindView(R.id.etxLoginPass)
    AppCompatEditText etxLoginPass;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.txtForgotPass)
    TextView txtForgotPass;
    @BindView(R.id.txtRegister)
    TextView txtRegister;
    @BindView(R.id.layoutLogin)
    LinearLayout layoutLogin;

    private Activity activity = LoginActivity.this;
    private SessionManager mySessionManager;
    private String email, password, fcmToken = "NA", fmToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        LinearLayout layoutLogin = findViewById(R.id.layoutLogin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);

        setTitle("");

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                fmToken = task.getResult().getToken();
                Log.d("FCM_TOKEN", "onComplete: " + fmToken);
                SharedPreferences.Editor editor = getSharedPreferences(WebService.SP_FCM_MAIN, MODE_PRIVATE).edit();
                editor.putString(WebService.FCM_TOKEN, fmToken);
                editor.apply();
                SharedPreferences preferences = getSharedPreferences(WebService.SP_FCM_MAIN, Context.MODE_PRIVATE);
                fcmToken = preferences.getString(WebService.FCM_TOKEN, fmToken);
                //Toast.makeText(activity, fmToken, Toast.LENGTH_SHORT).show();
            }
        });

        txtForgotPass.setPaintFlags(txtForgotPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtRegister.setPaintFlags(txtRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        mySessionManager = new SessionManager(activity);
        Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        if (layoutLogin.getVisibility() == View.GONE) {
            layoutLogin.startAnimation(slideUp);
            layoutLogin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @OnClick({R.id.btnLogin, R.id.txtForgotPass, R.id.txtRegister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                // startActivity(new Intent(activity, MainActivity.class));
                email = Objects.requireNonNull(etxLoginEmail.getText()).toString().trim();
                password = Objects.requireNonNull(etxLoginPass.getText()).toString().trim();
                if (onCheckInputs()) {
                    onAppLogin();
                }
                break;
            case R.id.txtForgotPass:
                // startActivity(new Intent(activity, ForgotPassword.class));

                break;
            case R.id.txtRegister:
                startActivity(new Intent(activity, RegisterActivity.class));
                break;
        }
    }


    private void onAppLogin() {

        Log.d("TAG", "onAppLogin: Hiiiiiii");
        final Dialog dialog = ConstantFunctions.onShowProgressDialog(activity);
        StringRequest loginRequest = new StringRequest(Request.Method.POST, WebService.API_LOGIN,
                response -> {
                    Log.d("TAG", "onAppLogin:  Response Hiiiiiii");
                    dialog.cancel();
                    onHandleResponse(response);
                }, error -> Log.d("TAG", "onErrorResponse: " + error.networkResponse)) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(WebService.PARAM_KEY_EMAIL, email);
                params.put(WebService.PARAM_KEY_PASSWORD, password);
                params.put(WebService.PARAM_KEY_FCM_TOKEN, fcmToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<>();
                param.put("API_KEY", WebService.API_KEY);
                return param;
            }
        };
        AppController.getInstance().addToRequestQueue(loginRequest);
    }

    private void onHandleResponse(String response) {
        Log.d("LOGIN_RESPONSE", "onHandleResponse: " + response);
        AuthResponseModel responseModel = new Gson().fromJson(response, AuthResponseModel.class);
        if (responseModel.getCode()) {
            String userId = responseModel.getResponse().getUserId();
            String userToken = responseModel.getResponse().getToken();
            String userName = responseModel.getResponse().getUserName();
            String userRole = responseModel.getResponse().getRole();
            mySessionManager.onCreateLogin(userToken, userId, userName, userRole);
            Toast.makeText(activity, responseModel.getMessage(), Toast.LENGTH_SHORT).show();

            if (userRole.equals("customer")) {
                startActivity(new Intent(activity, MainActivity.class));
            } else if (userRole.equals("admin")) {
                startActivity(new Intent(activity, AdminDashboard.class));
            }
            finish();
        } else {
            ConstantFunctions.OnShowAlertMessage(activity, responseModel.getMessage());
        }
    }


    private boolean onCheckInputs() {

        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter your valid Email address");
            return false;
        } else if (TextUtils.isEmpty(password)) {
            ConstantFunctions.OnShowAlertMessage(activity, "Enter your Password");
            return false;
        }
        return true;
    }

}
