package in.project.sensor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.project.sensor.adaptors.AllUsersAdaptors;
import in.project.sensor.utils.AppController;
import in.project.sensor.utils.SessionManager;
import in.project.sensor.utils.WebService;
import in.project.sensor.datamodels.userresponse.UserResponse;

public class AdminDashboard extends AppCompatActivity {

    @BindView(R.id.recyclerAllUsers)
    RecyclerView recyclerAllUsers;
    @BindView(R.id.swipeAllUsers)
    SwipeRefreshLayout swipeAllUsers;
    private Activity activity = AdminDashboard.this;
    private SessionManager mySessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        setTitle("Users");
        RecyclerView.LayoutManager cartLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerAllUsers.setLayoutManager(cartLayoutManager);
        recyclerAllUsers.hasFixedSize();
        recyclerAllUsers.setItemAnimator(new DefaultItemAnimator());
        recyclerAllUsers.setHasFixedSize(true);
        recyclerAllUsers.setItemViewCacheSize(500);
        mySessionManager = new SessionManager(activity);
        swipeAllUsers.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onGetUsers();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        onGetUsers();
    }

    private void onGetUsers() {
        String urlAddress = WebService.API_GET_USERS;
        StringRequest myOrderRequest = new StringRequest(Request.Method.GET, urlAddress,
                this::onSetUsers, error -> Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show()) {
        };
        AppController.getInstance().addToRequestQueue(myOrderRequest);
    }

    private void onSetUsers(String response) {
        UserResponse userResponse = new Gson().fromJson(response, UserResponse.class);
        if (userResponse.getCode()) {
            recyclerAllUsers.setAdapter(new AllUsersAdaptors(activity, userResponse.getData()));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            mySessionManager.onLogoutUser();
            startActivity(new Intent(activity, LoginActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
